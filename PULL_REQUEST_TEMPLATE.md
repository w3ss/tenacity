# Pull Requests

If you are submitting a pull request, please read https://codeberg.org/tenacityteam/tenacity/raw/branch/main/CONTRIBUTING.md
